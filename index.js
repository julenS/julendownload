const express = require('express');
const app = express();
const http = require('http');
const port = process.env.PORT || 3009;
const server = http.createServer(app);
const io = require('socket.io')();



//Mongo
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const router = require('./routes/router.js');
const mongodbRoute = "mongodb+srv://julentxi:cacahuetex120@julencluster0-acyde.mongodb.net/hobetuzdatabase?retryWrites=true&w=majority"



app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})
app.use(router);
/*MONGODB*/
const options = {
  socketTimeoutMS: 0,
  keepAlive: true,
  reconnectTries: 30,
  useNewUrlParser: true
};
mongoose.Promise = global.Promise
mongoose.connect(mongodbRoute, options, (err) => {
    if (err) {
        return console.log(`Error al conectar a la base de datos: ${err}`)
    }
    server.listen(port, () => {
      console.log(`Server activo en ${port}`);
	});
    console.log(`Conexión con Mongo correcta.`)
})

//Socket Configuration 
io.attach(server, {
  pingInterval: 2000,
  pingTimeout: 10000,
  cookie: false
})

io.on('connection', function(socket){
  socket.emit('pruebados', {"hi":"hola"})
  console.log(`user ${socket.id} connected`);
  socket.on('disconnect', function(){
    console.log("user has been disconnected")
  })
  socket.on('saludar', function(){
    socket.emit('resSaludar',{
      'message': `que te jodan, pailazo :) ${socket.id}`
    })
    console.log(`hola ${socket.id}`)
  })
})


