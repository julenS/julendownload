const express = require('express');
const router = express.Router();
const usersCntrl = require('../controllers/UsersController')


// Metodos

router.post('/', usersCntrl.postNewUser)
router.delete('/:userid', usersCntrl.deleteUser)
router.get('/', usersCntrl.getUsers)
router.post('/many/', usersCntrl.postManyUsers)
router.post('/many2/', usersCntrl.postManyUsers1)


module.exports = router;