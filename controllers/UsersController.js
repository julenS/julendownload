const User = require('../models/Users');


exports.postNewUser = function(req, res){
    let newUser = new User()
    Object.assign(newUser, req.body)
    newUser.save()
        .then((newUser)=>{
            console.log(newUser)
            res.send(newUser);
            return;
        })
        .catch((error) =>{
            res.status(400).send(error)
        })
    return;
}

exports.deleteUser = function(req, res){
    user = req.params.userid
    User.findOneAndDelete({_id:user}, function(err, result){
        if(err){return console.log(err), res.send(err)};
        if(!result){return console.log('No user to delete found'), res.status(404).send()};
        res.send(result)
    })
}

exports.getUsers = function(req, res){
    User.find({}, function(err, result){
        res.send(result)
    })
}

exports.postManyUsers1 = function(req,res){
    User.insertMany(req.body, function(err, response){
        if(err){console.log(err),res.send(err)}
        res.send(response)
    })
}

//notworking
exports.postManyUsers = function(req, res){
    let newUser = new User();
    req.body.map((item, item2)=> {
        Object.assign(newUser, item)
        newUser.save(i)
        .then((newUser)=>{
            console.log(newUser)
            return;
        })
        .catch((error)=>{
            console.log(error)
        })
    });
      
    res.status(200).send()
}