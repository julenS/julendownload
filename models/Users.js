const mongoose = require('mongoose');
const validator = require('validator');
// esto es solo pa pruebas222
//try3

const UserSchema = new mongoose.Schema({
    name: String,
    username: String,
    email: {
        type:String,
        validate: validator.isEmail,
        message:'{VALUE} is not a valid email',
    },
    avatar: String,
});

module.exports = mongoose.model('User', UserSchema)